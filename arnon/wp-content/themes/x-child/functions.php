<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );



// Additional Functions
// =============================================================================


function enqueueCSS(){
	wp_enqueue_style("adv-tabs","/wp-content/themes/x-child/advanced-tabs.css");
}

add_action("wp_enqueue_scripts","enqueueCSS");

function enqueueJS(){
    wp_enqueue_script("custom","/wp-content/themes/x-child/js/custom.js",array("jquery"),"1.0.0", FALSE);
}
add_action("wp_enqueue_scripts","enqueueJS");

// הוספת שדות לטופס הרשמה לאירוע  
// =============================================================================

function bweb_add_custom_event_fields(){
	?>
		<p>
			<label for='user_company'><?php esc_html_e('חברה', 'textdomain'); ?> * </label>
			<input type="text" name="user_company" id="user-company"  class="input" value="<?php if(!empty($_REQUEST['user_company'])) echo esc_attr($_REQUEST['user_company']); ?>" />
		</p>
        <p>
			<label for='user_pos'><?php esc_html_e('תפקיד', 'textdomain'); ?> * </label>
			<input type="text" name="user_pos" id="user-pos"  class="input" value="<?php if(!empty($_REQUEST['user_pos'])) echo esc_attr($_REQUEST['user_pos']); ?>" />
		</p>
        <p>
			<label for='user_inv'><?php esc_html_e('מזמין', 'textdomain'); ?></label>
			<input type="text" name="user_inv" id="user-inv"  class="input" value="<?php if(!empty($_REQUEST['user_inv'])) echo esc_attr($_REQUEST['user_inv']); ?>" />
		</p>

	<?php
}
add_action('em_register_form','bweb_add_custom_event_fields');


function  bweb_save_custom_event_fields (){
	global $EM_Booking ;
	if( ! empty( $_REQUEST['user_company'] ) ){
			$EM_Booking->booking_meta['registration']['user_company'] = wp_kses( $_REQUEST['user_company'], array() );
	}
    if( ! empty( $_REQUEST['user_pos'] ) ){
			$EM_Booking->booking_meta['registration']['user_pos'] = wp_kses( $_REQUEST['user_pos'], array() );
	}
    if( ! empty( $_REQUEST['user_inv'] ) ){
			$EM_Booking->booking_meta['registration']['user_inv'] = wp_kses( $_REQUEST['user_inv'], array() );
	}
}
add_filter('em_booking_add','bweb_save_custom_event_fields');	


function bweb_table_custom_event_fields($template, $EM_Bookings_Table){
        $template['user_company'] = __('חברה', 'textdomain');
        $template['user_pos'] = __('תפקיד', 'textdomain');
        $template['user_inv'] = __('מזמין', 'textdomain');
        return $template;
}
add_action('em_bookings_table_cols_template', 'bweb_table_custom_event_fields',10,2);
 
 
function bweb_display_col_custom_event_fields($val, $col, $EM_Booking, $EM_Bookings_Table, $csv){
        if( $col == 'user_company' ){
                $val = $EM_Booking->get_person()->user_company;               
        }
        if( $col == 'user_pos' ){
                $val = $EM_Booking->get_person()->user_pos;               
        }
        if( $col == 'user_inv' ){
                $val = $EM_Booking->get_person()->user_inv;               
        }
        return $val;
}
add_filter('em_bookings_table_rows_col','bweb_display_col_custom_event_fields', 10, 5);

// וידוא שדות חובה
//===============================================================

function em_validate($result, $EM_Event){
if (!is_user_logged_in() && $_REQUEST['user_name'] == ''){
    $EM_Event->add_error('אנא מלא/י את שמך המלא..');
    $result = false;
}
if (!is_user_logged_in() && $_REQUEST['user_email'] == ''){
    $EM_Event->add_error('אנא מלא/י כתובת מייל עדכנית..');
    $result = false;
}
if (!is_user_logged_in() && $_REQUEST['user_company'] == ''){
    $EM_Event->add_error('אנא מלא/י את שם החברה..');
    $result = false;
}
if (!is_user_logged_in() && $_REQUEST['user_pos'] == ''){
    $EM_Event->add_error('אנא מלא/י את תפקידך בחברה..');
    $result = false;
}
return $result;
}
add_filter('em_booking_validate','em_validate', 1, 2);