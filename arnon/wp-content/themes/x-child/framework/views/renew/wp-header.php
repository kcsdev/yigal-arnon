<?php

// =============================================================================
// /x-child/framework/views/renew/WP-HEADER.PHP
// -----------------------------------------------------------------------------
// Header output for Renew.
// =============================================================================

?>

<?php x_get_view( 'global', '_header' ); ?>

  <?php x_get_view( 'global', '_slider-above' ); ?>

  <header class="<?php x_masthead_class(); ?>" role="banner">
    <?php x_get_view( 'global', '_topbar' ); ?>
    <?php x_get_view( 'global', '_navbar' ); ?>
  </header>

        <?php
            if( get_field('profile_pic'))
        {
         ?>   <div class='main_banner_wrap'>
                    <div class='inside-content'>
                        <img src="<?php the_field('profile_pic'); ?>" />
                        <ul>
                            <li  class="lawyer_name"> <?php  echo get_field('lawyer_name');?></li>
                            <li  class="lawyer_position"> <?php  echo get_field('lawyer_position');?></li>
                            <i class="fa fa-map-marker"></i><li  class="lawyer_address"> <?php  echo get_field('lawyer_address');?></li>
                            <i class="fa fa-mobile"></i><li  class="lawyer_phone"> <?php  echo get_field('lawyer_phone');?><i class="fa fa-print"></i><?php  echo get_field('lawyer_fax');?></li>
                            <i class="fa fa-envelope-o"></i><a href="mailto:<?php  echo get_field('lawyer_mail');?>"><li  class="lawyer_mail"> <?php  echo get_field('lawyer_mail');?></a></li>
                            <i class="fa fa-globe"></i><li  class="lawyer_lang"> <?php  echo get_field('lawyer_lang');?></li>
                        </ul>

                    </div>
              </div><?php;
          };?>

        <?php if( get_field('banner_pic')): ?>   
            <div class='top_banner_wrap'>
                <img src="<?php the_field('banner_pic'); ?>" />
                <?php if( get_field('head-color') == 'אדום'): ?>
                        <h2 style="position: absolute; top: 235px; left: 0px; width: 100%; text-align: center; font-size: 72px; font-weight: 300; color: #7c1518;">
                            <?php echo get_the_title(); ?>
                        </h2>
                <?php else:  ?>
                        <h2>
                            <?php echo get_the_title(); ?>
                        </h2>
                   <?php endif; ?>
             </div>
        <?php endif; ?>

  <?php x_get_view( 'global', '_slider-below' ); ?>
  <?php x_get_view( 'renew', '_landmark-header' ); ?>