<?php

// =============================================================================
// VIEWS/RENEW/WP-FOOTER.PHP
// -----------------------------------------------------------------------------
// Footer output for Renew.
// =============================================================================

?>
    <?php 
        {
        ?>   <div class='newsletter-widget-wrap'>
                <div class='newsletter-widget'>
                   <?php dynamic_sidebar('newsletter-widget');?> 
                </div>
             </div><?php;
        };?>

  <?php x_get_view( 'global', '_header', 'widget-areas' ); ?>
  <?php x_get_view( 'global', '_footer', 'scroll-top' ); ?>
  <?php x_get_view( 'global', '_footer', 'widget-areas' ); ?>

  <?php if ( x_get_option( 'x_footer_bottom_display', '1' ) == '1' ) : ?>

    <footer class="x-colophon bottom" role="contentinfo">
      <div class="x-container max width">

        <?php if ( x_get_option( 'x_footer_social_display', '1' ) == '1' ) : ?>
          <?php x_social_global(); ?>
        <?php endif; ?>

        <?php if ( x_get_option( 'x_footer_menu_display', '1' ) == '1' ) : ?>
          <?php x_get_view( 'global', '_nav', 'footer' ); ?>
        <?php endif; ?>

<!--
        <?php if ( x_get_option( 'x_footer_content_display', '1' ) == '1' ) : ?>
          <div class="x-colophon-content">
            <?php echo do_shortcode( x_get_option( 'x_footer_content' ) ); ?>
          </div>
        <?php endif; ?>
-->
          <p style="float: right;"><i class="fa fa-copyright"></i> כל הזכויות שמורות למשרד עו"ד יגאל ארנון בע"מ</p>
<p style="float: left;">
    <a href="http://www.kcsnet.net/"><img src="/arnon/wp-content/uploads/logo/New-Logo.png" width="45">
    <span style="float:right; line-height:1.6; margin-left:5px;"> 
        בניית אתרים</a>
    </span>
</p>

      </div>
    </footer>

  <?php endif; ?>

<?php x_get_view( 'global', '_footer' ); ?>