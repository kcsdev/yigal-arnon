<?php

// =============================================================================
// VIEWS/GLOBAL/_TOPBAR.PHP
// -----------------------------------------------------------------------------
// Includes topbar output.
// =============================================================================

?>

<?php if ( x_get_option( 'x_topbar_display', '' ) == '1' ) : ?>

  <div class="x-topbar">
    <div class="x-topbar-inner x-container max width">
        <div class="access">
            <span style="float: right; margin-top: 2px; margin-left: 10px;"><strong>נגישות:</strong></span><?php echo do_shortcode('[wpa_toolbar]')?>
        </div>
        <div class="topbar-social">
            <a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-linkedin"></i></a>
        </div>
<!--
        <div class="searchform">
      	<a href="#" class="x-btn-navbar-search"><span><i class="x-icon x-icon-search"></i><span class="x-hidden-desktop">Search</span></span></a>
        </div>
-->
        <?php if ( x_get_option( 'x_topbar_content' ) != '' ) : ?>
      <p class="p-info"><?php echo x_get_option( 'x_topbar_content' ); ?></p>
      <?php endif; ?>
    <!--      <?php x_social_global(); ?>-->
    </div>
  </div>

<?php endif; ?>