<? php
/**
 * Template Name: Lawyer
 *
 */
;?>

<?php get_header(); ?>

  <div class="x-container max width offset">
      
    <div class="<?php x_main_content_class(); ?>" role="main">

      <?php while ( have_posts() ) : the_post(); ?>
        <?php x_get_view( 'renew', 'content', 'page' ); ?>
        <?php x_get_view( 'global', '_comments-template' ); ?>
      <?php endwhile; ?>

    </div>
    <div class="lawyer-sidebar">
         <div class="lawyer-cite">
             <span class="cite-text"><?php echo get_field('cite')?></span>
             <br />
             <span class="cite-name"><?php echo get_field('lawyer_name')?></h6>
         </div>
         <div class="lawyer-services">
             <h4>שירותים משפטיים</h4>
             <?php 
                $posts = get_field('legal_services');
                if( $posts ): ?>
                    <ul style="list-style: none;">
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                        <?php setup_postdata($post); ?>
                        <li>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
         </div>
         <div class="lawyer-sectors">
             <h4>מגזרים</h4>
             <?php 
                $posts = get_field('lawyer_sectors');
                if( $posts ): ?>
                    <ul style="list-style: none;">
                    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                        <?php setup_postdata($post); ?>
                        <li>
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
          </div>
        
    </div>
  </div>

<?php get_footer(); ?>