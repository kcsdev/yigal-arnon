<?php get_header(); ?>

  <div class="x-container max width offset">
    <div class="<?php x_main_content_class(); ?>" role="main" style="float: right;">

      <?php while ( have_posts() ) : the_post(); ?> 
        <div>
            <h1 class="entry-title" style="text-decoration: underline;"><?php echo $EM_Event->output('#_EVENTNAME'); ?></h1><br />
            <?php if ( has_post_thumbnail() ) : ?>
              <div class="entry-featured mtn">
                <?php x_featured_image(); ?>
              </div>
            <?php endif; ?><br />
            <div style="float:left;"><?php echo $EM_Event->output('#_LOCATIONMAP'); ?><h3>הזמנות</h3>
            <?php echo $EM_Event->output(' #_BOOKINGFORM'); ?>
            </div>
            <p style="margin: 0;"><strong>תקציר</strong>
                <?php echo $EM_Event->output('#_EVENTEXCERPT'); ?>
            </p>
            <p><strong>תאריך/זמן</strong><br/>
                <?php echo $EM_Event->output('#_EVENTDATES'); ?><br/>
                <span>מ- </span><?php echo $EM_Event->output('#_24HSTARTTIME'); ?><span> עד- </span>
                <?php echo $EM_Event->output('#_24HENDTIME'); ?>
            </p>
            <p><strong>מקום</strong><br/>
                <?php echo $EM_Event->output('#_LOCATIONLINK'); ?>
            </p>
            <p style="margin: 0;"><strong>תחומים קשורים</strong>
            <?php echo $EM_Event->output('#_EVENTCATEGORIES'); ?>
            </p>
            <?php
                if (get_field('related_lawyers')) { 
                         ?>
                <p><strong>עו"ד קשורים</strong><br />
                    <?php
                        $post_objects = get_field('related_lawyers');
                        if( $post_objects ): ?>

                            <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                                <?php setup_postdata($post); ?>

                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    <span><?php the_field('related_lawyers'); ?> ,</span>

                            <?php endforeach; ?>

                            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                        <?php endif;?>
                </p>
            <?php } 
            ?>
            <?php
                if (get_field('event_gal')) {
                        ?>
                <div class="event-gallery"><p><strong>גלריות</strong></p>
                    <?php
                            foreach ( get_field ( 'event_gal' ) as $nextgen_gallery_id ) :
                                if ( $nextgen_gallery_id['ngg_form'] == 'album' ) {
                                    echo nggShowAlbum( $nextgen_gallery_id['ngg_id'] ); //NextGEN Gallery album
                                } elseif ( $nextgen_gallery_id['ngg_form'] == 'gallery' ) {
                                     echo nggShowGallery( $nextgen_gallery_id['ngg_id'] ); //NextGEN Gallery gallery
                                }
                            endforeach;?>
                </div>
            <?php }
            ?>
        </div>
        <?php x_get_view( 'global', '_comments-template' ); ?>
      <?php endwhile; ?>

    </div>
      
    <div class="<?php x_sidebar_class(); ?>" role="complementary">
      <?php dynamic_sidebar('ups-sidebar-events'); ?>
    </div>

  </div>
    

<?php get_footer(); ?>